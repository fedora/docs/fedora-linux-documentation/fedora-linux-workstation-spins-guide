# Fedora Linux Workstation and Spins User Guide
## DRAFTs

Interim version for the index including an elaboration of the guide's approach: https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/blob/main/modules/ROOT/pages/index.adoc

Details of the approach of the new Web UI Guide: https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/blob/main/modules/ROOT/pages/WebUI_Install_Guide.adoc

Example of how a page of this guide might look like / be structured: https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/blob/main/modules/ROOT/pages/BackUp.adoc

Current draft for the navigation menu:  https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/blob/main/modules/ROOT/nav.adoc

Current draft for first page of the Web UI guide (not tackling the Web UI itself but the steps before): https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/blob/main/modules/ROOT/pages/why-fedora-which-image.adoc

Current draft for second page of the Web UI guide (not tackling the Web UI itself but the steps before): https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/blob/main/modules/ROOT/pages/create-installationmedia.adoc

Also consider [MAINTAINER.md](https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/blob/main/MAINTAINER.md)

## Branching & Fedora releases

Currently, it is planned to have a branch for each Fedora release, and a main branch for the current development towards the next release. However, updates/changes have to be applied to all affected branches, but only to those that are still supported (e.g., on 25. September 2022, this could be main, F36 and F35). Additional branches might appear for content that has the "offline" status as elaborated in the [MAINTAINER.md](https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/blob/main/MAINTAINER.md).  

## Local preview

### Installing Podman and git on Fedora

Fedora Workstation doesn't come with Podman preinstalled by default — so you might need to install it using the following command:

```
$ sudo dnf install podman
```

### Preview as a part of the whole Fedora Docs site

You can also build the whole Fedora Docs site locally to see your changes in the whole context.
This is especially useful for checking if your `xref` links work properly.

Steps:

Clone the main repository and cd into it:

```
$ git clone https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide
$ cd fedora-linux-workstation-spins-guide
```

To build the whole site, I would run the following in the `fedora-linux-workstation-spins-guide` directory.

```
$ ./build.sh && ./preview.sh
```

The result will be available at http://localhost:8080  

You can use the following links to directly get to the respective preview of the drafts mentioned above:  
 - [Index with approach of the guide](http://localhost:8080/pizza-factory/)  
 - [Page example: BackUp](http://localhost:8080/pizza-factory/BackUp/) (Important tasks and automation -> BackUp, snapshots and RAID)  
 - The "Web UI Installation Guide" page currently only drafts the approach and tracks the development and status. It contains no content that is intended for presentation on Docs pages: at the menu entry "Choose and install Workstation or spins", it will be replaced once a draft for the guide is in development.  

# License

SPDX-License-Identifier: CC-BY-SA-4.0
